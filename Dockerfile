FROM openjdk:8u111-jdk-alpine

ADD / /gatling
WORKDIR gatling

ENTRYPOINT ["sh","./bin/gatling.sh","-s","fileupload.FileResizeSimulation"]


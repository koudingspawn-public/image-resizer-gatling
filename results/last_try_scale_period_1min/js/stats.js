var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "500",
        "ok": "498",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "220",
        "ok": "532",
        "ko": "220"
    },
    "maxResponseTime": {
        "total": "1638",
        "ok": "1638",
        "ko": "340"
    },
    "meanResponseTime": {
        "total": "687",
        "ok": "688",
        "ko": "280"
    },
    "standardDeviation": {
        "total": "126",
        "ok": "124",
        "ko": "60"
    },
    "percentiles1": {
        "total": "670",
        "ok": "671",
        "ko": "280"
    },
    "percentiles2": {
        "total": "713",
        "ok": "713",
        "ko": "310"
    },
    "percentiles3": {
        "total": "849",
        "ok": "850",
        "ko": "334"
    },
    "percentiles4": {
        "total": "1338",
        "ok": "1339",
        "ko": "339"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 466,
        "percentage": 93
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 24,
        "percentage": 5
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 8,
        "percentage": 2
    },
    "group4": {
        "name": "failed",
        "count": 2,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.962",
        "ok": "0.958",
        "ko": "0.004"
    }
},
contents: {
"req_request-1-46da4": {
        type: "REQUEST",
        name: "request_1",
path: "request_1",
pathFormatted: "req_request-1-46da4",
stats: {
    "name": "request_1",
    "numberOfRequests": {
        "total": "500",
        "ok": "498",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "220",
        "ok": "532",
        "ko": "220"
    },
    "maxResponseTime": {
        "total": "1638",
        "ok": "1638",
        "ko": "340"
    },
    "meanResponseTime": {
        "total": "687",
        "ok": "688",
        "ko": "280"
    },
    "standardDeviation": {
        "total": "126",
        "ok": "124",
        "ko": "60"
    },
    "percentiles1": {
        "total": "670",
        "ok": "671",
        "ko": "280"
    },
    "percentiles2": {
        "total": "713",
        "ok": "713",
        "ko": "310"
    },
    "percentiles3": {
        "total": "849",
        "ok": "850",
        "ko": "334"
    },
    "percentiles4": {
        "total": "1338",
        "ok": "1339",
        "ko": "339"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 466,
        "percentage": 93
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 24,
        "percentage": 5
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 8,
        "percentage": 2
    },
    "group4": {
        "name": "failed",
        "count": 2,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.962",
        "ok": "0.958",
        "ko": "0.004"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}

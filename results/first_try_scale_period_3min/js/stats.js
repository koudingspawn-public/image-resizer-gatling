var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "500",
        "ok": "496",
        "ko": "4"
    },
    "minResponseTime": {
        "total": "291",
        "ok": "526",
        "ko": "291"
    },
    "maxResponseTime": {
        "total": "2141",
        "ok": "2141",
        "ko": "1119"
    },
    "meanResponseTime": {
        "total": "731",
        "ok": "733",
        "ko": "521"
    },
    "standardDeviation": {
        "total": "211",
        "ok": "209",
        "ko": "346"
    },
    "percentiles1": {
        "total": "668",
        "ok": "668",
        "ko": "338"
    },
    "percentiles2": {
        "total": "741",
        "ok": "741",
        "ko": "542"
    },
    "percentiles3": {
        "total": "1131",
        "ok": "1132",
        "ko": "1004"
    },
    "percentiles4": {
        "total": "1609",
        "ok": "1611",
        "ko": "1096"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 398,
        "percentage": 80
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 81,
        "percentage": 16
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 17,
        "percentage": 3
    },
    "group4": {
        "name": "failed",
        "count": 4,
        "percentage": 1
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.954",
        "ok": "0.947",
        "ko": "0.008"
    }
},
contents: {
"req_request-1-46da4": {
        type: "REQUEST",
        name: "request_1",
path: "request_1",
pathFormatted: "req_request-1-46da4",
stats: {
    "name": "request_1",
    "numberOfRequests": {
        "total": "500",
        "ok": "496",
        "ko": "4"
    },
    "minResponseTime": {
        "total": "291",
        "ok": "526",
        "ko": "291"
    },
    "maxResponseTime": {
        "total": "2141",
        "ok": "2141",
        "ko": "1119"
    },
    "meanResponseTime": {
        "total": "731",
        "ok": "733",
        "ko": "521"
    },
    "standardDeviation": {
        "total": "211",
        "ok": "209",
        "ko": "346"
    },
    "percentiles1": {
        "total": "668",
        "ok": "668",
        "ko": "338"
    },
    "percentiles2": {
        "total": "741",
        "ok": "741",
        "ko": "542"
    },
    "percentiles3": {
        "total": "1131",
        "ok": "1132",
        "ko": "1004"
    },
    "percentiles4": {
        "total": "1609",
        "ok": "1611",
        "ko": "1096"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 398,
        "percentage": 80
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 81,
        "percentage": 16
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 17,
        "percentage": 3
    },
    "group4": {
        "name": "failed",
        "count": 4,
        "percentage": 1
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.954",
        "ok": "0.947",
        "ko": "0.008"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}

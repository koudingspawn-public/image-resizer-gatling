var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "500",
        "ok": "37",
        "ko": "463"
    },
    "minResponseTime": {
        "total": "35",
        "ok": "579",
        "ko": "35"
    },
    "maxResponseTime": {
        "total": "2252",
        "ok": "2252",
        "ko": "1610"
    },
    "meanResponseTime": {
        "total": "200",
        "ok": "1211",
        "ko": "119"
    },
    "standardDeviation": {
        "total": "338",
        "ok": "500",
        "ko": "122"
    },
    "percentiles1": {
        "total": "91",
        "ok": "1078",
        "ko": "87"
    },
    "percentiles2": {
        "total": "130",
        "ok": "1702",
        "ko": "122"
    },
    "percentiles3": {
        "total": "988",
        "ok": "2038",
        "ko": "308"
    },
    "percentiles4": {
        "total": "1884",
        "ok": "2212",
        "ko": "590"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 11,
        "percentage": 2
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 11,
        "percentage": 2
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 15,
        "percentage": 3
    },
    "group4": {
        "name": "failed",
        "count": 463,
        "percentage": 93
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.963",
        "ok": "0.071",
        "ko": "0.892"
    }
},
contents: {
"req_request-1-46da4": {
        type: "REQUEST",
        name: "request_1",
path: "request_1",
pathFormatted: "req_request-1-46da4",
stats: {
    "name": "request_1",
    "numberOfRequests": {
        "total": "500",
        "ok": "37",
        "ko": "463"
    },
    "minResponseTime": {
        "total": "35",
        "ok": "579",
        "ko": "35"
    },
    "maxResponseTime": {
        "total": "2252",
        "ok": "2252",
        "ko": "1610"
    },
    "meanResponseTime": {
        "total": "200",
        "ok": "1211",
        "ko": "119"
    },
    "standardDeviation": {
        "total": "338",
        "ok": "500",
        "ko": "122"
    },
    "percentiles1": {
        "total": "91",
        "ok": "1078",
        "ko": "87"
    },
    "percentiles2": {
        "total": "130",
        "ok": "1702",
        "ko": "122"
    },
    "percentiles3": {
        "total": "988",
        "ok": "2038",
        "ko": "308"
    },
    "percentiles4": {
        "total": "1884",
        "ok": "2212",
        "ko": "590"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 11,
        "percentage": 2
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 11,
        "percentage": 2
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 15,
        "percentage": 3
    },
    "group4": {
        "name": "failed",
        "count": 463,
        "percentage": 93
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0.963",
        "ok": "0.071",
        "ko": "0.892"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
